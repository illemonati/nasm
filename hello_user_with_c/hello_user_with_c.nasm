
section .bss ;variable
    name resb 255

section .text ;program
    extern printf
    extern scanf
    global main

main:
    push rbp
    mov rbp, rsp

    mov rdi, msg_hello
    mov rsi, string_fmt
    mov rax, 0
    call printf

    call get_name

    call print_back_name

    mov rax, 0
    pop rbp
    ret

get_name:
    push rbp
    mov rbp, rsp

    mov rdi, msg_get_name
    xor rax, rax
    call printf

    mov rdi, string_fmt
    mov rsi, name
    xor rax, rax
    call scanf

    mov rax, 0
    pop rbp
    ret

print_back_name:
    push rbp
    mov rbp, rsp

    mov rdi, print_back_name_fmt
    mov rsi, name
    mov rax, 0
    call printf

    pop rbp
    ret



section .data ;constant
    msg_hello: db "Hello, User!", 10, 0
    string_fmt: db "%s", 0

    msg_get_name: db "What is Your name? ", 10, 0

    print_back_name_fmt: db "Hello! Lord and Master %s !", 10, 0