section .data
    name_prompt    db "What is your name? "
    name_prompt_len    equ $-name_prompt
    hello  db "Hello, "
    hello_len  equ $-hello

section .bss
    name    resb 255

section .text
    global _start

_start:
;    mov eax, 4
;    mov ebx, 1
;    mov ecx, name_prompt
;    mov edx, name_prompt_len
;    int 80h

    mov esi, name_prompt
    mov edi, name_prompt_len
    call print

    mov eax, 3
    mov ebx, 0
    mov ecx, name
    mov edx, 255
    int 80h

    mov esi, hello
    mov edi, hello_len
    call print

    mov esi, name
    mov edi, 255
    call print


    jmp _exit


print:
    mov eax, 4
    mov ebx, 1
    mov ecx, esi
    mov edx, edi
    int 80h
    ret

_exit:
    mov eax, 1
    mov ebx, 0
    int 80h


