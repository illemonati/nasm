section .data ;constant
    msg db "Hello, NASM!", 10
    msg_len equ $-msg
section .bss ;variable

section .text ;program
    global _start

_start:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg
    mov edx, msg_len
    int 80h

    mov eax, 1
    mov ebx, 0
    int 80h