
section .text
global main
extern printf
%define int_arr_get(arr, i) [arr+i*4]

struc simple_structure
    .a  resd 1
    .b  resd 1
endstruc

main:
    push    rbp
    mov     rbp, rsp

    mov     rdi, num_fmt
    mov     rsi, int_arr_get(int_array1, 3)
    call    printf

    mov     rdi, num_fmt
    mov     rsi, [simple_structure1+simple_structure.b]
    call    printf

    mov     rax, 0
    pop     rbp
    ret


section .bss


section .data
    num_fmt:                    db "%d", 10, 0
    simple_int32:               dd 32
    int_array1:                 dd 1, 2, 3, 4, 5
    int_array_2:                db 1, 0, 0, 0
                                db 2, 0, 0, 0
                                db 3, 0, 0, 0
                                db 4, 0, 0, 0
                                db 5, 0, 0, 0

    simple_structure1:
        istruc  simple_structure
        at  simple_structure.a, dd 234
        at  simple_structure.b, dd 519
        iend