





section .data
start: db "Start!", 10, 0
num_format: db "Number is %d", 10, 0
num: dq 1000000


section .bss


section .text
extern printf
global main
global _counting_loop

main:
    push rbp
    mov rbp, rsp

    mov rdi, start
    mov rax, 0
    call printf

    mov rdi, [num]
    call counting_loop

    pop rbp
    ret

counting_loop:
    push rbp
    mov rbp, rsp

    push rdi
    push qword 0

.l:
    mov rdi, num_format
    mov rsi, [rbp - 16]
    call printf
    inc qword [rbp - 16]

    mov rcx, [rbp-16]
    cmp rcx, qword [rbp - 8]
    jle .l

    mov eax, 0
    mov rsp, rbp
    pop rbp
    ret
