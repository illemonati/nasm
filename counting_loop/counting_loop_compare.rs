
fn main() {
    println!("start");
    counting_loop();
}

#[no_mangle]
fn counting_loop(){
    let mut a: i32 = 0;
    while &mut a < &mut 1000000  {
        a += 1;
        println!("Number is {}", a);
    }
}